extends KinematicBody2D

export var GRAVITY = 100.0
export var WALK_SPEED = 200
export var JUMP_HEIGHT = 2000000
export var LIFE = 3
var velocity = Vector2()
var canJump = false
var lastEnemy = null
var wait = false

func _physics_process(delta):
	if wait:
		return
	# Simulamos la gravedad
	if is_on_floor() or is_on_leader():
		velocity.y = 0
		canJump = true
	else:
		velocity.y += delta * GRAVITY		
		canJump = false
	
	if canJump:
		if Input.is_action_pressed("ui_left"):
			velocity.x = -WALK_SPEED
			$AnimatedSprite.animation = "walk"
			$AnimatedSprite.flip_h = true
		elif Input.is_action_pressed("ui_right"):
			velocity.x =  WALK_SPEED
			$AnimatedSprite.animation = "walk"
			$AnimatedSprite.flip_h = false
		else:
			velocity.x = 0
			if not Input.is_action_pressed("ui_up") and not Input.is_action_pressed("ui_down"):
				$AnimatedSprite.animation = "idle"
		
	if Input.is_action_pressed("ui_up") and canJump:
		if not is_on_leader():
			velocity.y = -JUMP_HEIGHT
			$AnimatedSprite.animation = "jump"
			canJump = false	
		else:
			$AnimatedSprite.animation = "climb"
			velocity.y = -WALK_SPEED / 2
			
	if Input.is_action_pressed("ui_down") and canJump:
		if not is_on_leader():
			velocity.y = -JUMP_HEIGHT
			$AnimatedSprite.animation = "jump"
			canJump = false	
		else:
			$AnimatedSprite.animation = "climb"
			velocity.y = -WALK_SPEED / -2
			
	for raycast in [$RayCast2D, $RayCast2D2]:
		if raycast.is_colliding():		
			var collider = raycast.get_collider()
			if collider != null:
				if collider.is_in_group('Enemies'):
					velocity.y = -JUMP_HEIGHT
					$AnimatedSprite.animation = "jump"
					collider.destroy()
					print ("Colliding with ", collider.name)
				if collider.name == "DangerTileMap":
					velocity.y = -JUMP_HEIGHT
					destroy()
					print ("Mata al jugador")
					
	
	
	move_and_slide(velocity, Vector2(0, -1))

func is_on_leader():
	if $RayCast2Leaders.is_colliding():
		var collider = $RayCast2Leaders.get_collider()
		if collider != null and collider.name == "TileMapLeader":
			return true
	return false
	
func hit(killer):
	if (killer != lastEnemy):
		wait = true
		lastEnemy = killer
		LIFE -= 1
		if (LIFE == 0):
			destroy()
		else:
			$WaitTimer.start()
			$AnimatedSprite.play("death")
		
func _on_WaitTimer_timeout():
	wait = false
	lastEnemy = null
	$AnimatedSprite.play("idle")

func destroy():
	$AnimatedSprite.play("death")
	$DeathTimer.start()

func _on_DeathTimer_timeout():
	get_tree().reload_current_scene()
	
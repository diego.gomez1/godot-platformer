extends KinematicBody2D

const GRAVITY = 10
const SPEED = 30
const FLOOR = Vector2(0,-1)
var velocity = Vector2()
var direction = 1
var death = false

func _ready():
	add_to_group('Enemies')

func _physics_process(delta):
	if !death:
		velocity.x = SPEED * direction	
		
		if direction > 0:
			$AnimatedSprite.flip_h = true
			$RayCast2D.position.x = 16
		else:
			$AnimatedSprite.flip_h = false
			$RayCast2D.position.x = -16
		
		$AnimatedSprite.play('walk')
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, FLOOR)
		
		if is_on_wall():
			direction = direction * -1
			$RayCast2D.position.x *= -1
		
		if $RayCast2D.is_colliding() == false:
			direction *= -1
			$RayCast2D.position.x *= -1
			
		if $KillRaycast2D.is_colliding():		
			var collider = $KillRaycast2D.get_collider()
			if collider.name == 'Player':
				collider.hit(self)

func destroy():
	if !death:
		death = true
		$AnimatedSprite.play('death')
		$Timer.start()

func _on_Timer_timeout():
	queue_free()
extends Sprite

export(NodePath) var nextLevel = null

func _on_Area2D_body_entered(body):
	print("Acaba de entrar por la puerta: ", body.name)
	get_tree().change_scene(nextLevel)
	
